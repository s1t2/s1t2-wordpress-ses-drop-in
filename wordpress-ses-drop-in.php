<?php
/*
  Plugin Name: SES DropIn
  Version: 1.3
*/

use Aws\Ses\SesClient;
use Aws\Exception\AwsException;

if ( ! function_exists( 'wp_mail' ) && defined( 'ABSPATH' ) ) {
	include_once ABSPATH . 'wp-includes/class-phpmailer.php';

	function wp_mail( $recipients, $subject, $message, $headers = array(), $attachments = array() ) {
		$mail = new PHPMailer();

		// obtain default sender from db
		$admin_email = get_bloginfo( 'admin_email' );
		$blog_name   = html_entity_decode( get_bloginfo( 'name' ), ENT_QUOTES, 'UTF-8' );

		// attempt to extract sender from headers
		if ( isset( $headers['From'] ) && preg_match( '/From:\s*\\"(.*[^\s])\\"\s*<\s*(.*[^\s])\s*>/', $headers['From'], $matches ) ) {
			$from_email = $matches[2];
			$from_name  = $matches[1] === $from_email ? $blog_name : $matches[1];

			// check if sender email is under the same domain as admin email
			// use defaults if not
			$admin_email_domain = end( explode( '@', $admin_email ) );
			$from_email_domain  = end( explode( '@', $from_email ) );
			if ( $admin_email_domain !== $from_email_domain ) {
				$from_email = $admin_email;
				$from_name  = $blog_name;
			}
		} else {
			$from_email = $admin_email;
			$from_name  = $blog_name;
		}
		$mail->setFrom( $from_email, $from_name );

		$recipients = array_unique( is_array( $recipients ) ? $recipients : explode( ',', $recipients ) );
		foreach ( $recipients as $recipient ) {
			if ( $recipient === $admin_email && isset( $_ENV['AWS_SES_RECIPIENT'] ) ) {
				$mail->addAddress( $_ENV['AWS_SES_RECIPIENT'] );
			} else {
				$mail->addAddress( $recipient );
			}
		}

		$mail->isHTML( true );
		$mail->Subject = $subject;
		$mail->Body    = $message;

		foreach ( $attachments as $attachment ) {
			$mail->addAttachment( $attachment );
		}

		if ( ! $mail->preSend() ) {
			throw new Exception( $mail->ErrorInfo );
		}

		$rawMessage = $mail->getSentMIMEMessage();

		try {
			$args = array(
				'version' => 'latest',
				'region'  => isset( $_ENV['AWS_SES_REGION'] ) ? $_ENV['AWS_SES_REGION'] : 'ap-southeast-2',
			);
			if ( isset( $_ENV['AWS_ACCESS_KEY_ID'] ) && isset( $_ENV['AWS_SECRET_ACCESS_KEY'] ) ) {
				$args = array_merge(
					$args,
					array(
						'credentials' => array(
							'key'    => $_ENV['AWS_ACCESS_KEY_ID'],
							'secret' => $_ENV['AWS_SECRET_ACCESS_KEY'],
						),
					)
				);
			}

			$client = new SesClient( $args );
			return $client->sendRawEmail(
				array(
					'RawMessage' => array(
						'Data' => $rawMessage,
					),
				)
			);
		} catch ( AwsException $ex ) {
			throw new Exception( $ex->getMessage() );
		}
	}
}
